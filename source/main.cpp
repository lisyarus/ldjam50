#include <psemek/app/app.hpp>
#include <psemek/app/main.hpp>

#include <psemek/gfx/gl.hpp>
#include <psemek/gfx/program.hpp>
#include <psemek/gfx/mesh.hpp>
#include <psemek/gfx/color.hpp>
#include <psemek/gfx/painter.hpp>
#include <psemek/gfx/pixmap.hpp>

#include <psemek/pcg/perlin.hpp>

#include <psemek/random/generator.hpp>
#include <psemek/random/device.hpp>
#include <psemek/random/uniform.hpp>
#include <psemek/random/uniform_sphere.hpp>

#include <psemek/geom/camera.hpp>
#include <psemek/geom/swizzle.hpp>
#include <psemek/geom/translation.hpp>
#include <psemek/geom/scale.hpp>
#include <psemek/geom/contains.hpp>
#include <psemek/geom/ray.hpp>
#include <psemek/geom/intersection.hpp>
#include <psemek/geom/homogeneous.hpp>

#include <psemek/util/clock.hpp>
#include <psemek/util/array.hpp>
#include <psemek/util/enum.hpp>

#include <psemek/io/memory_stream.hpp>

#include <psemek/audio/engine.hpp>

#include <models/select.hpp>
#include <models/forest.hpp>
#include <models/forest_shadow.hpp>
#include <models/stone.hpp>
#include <models/fishing_hut.hpp>
#include <models/farm.hpp>
#include <models/woodcutter.hpp>
#include <models/woodcutter_shadow.hpp>
#include <models/stone_quarry.hpp>
#include <models/temple.hpp>

#include <cards/unknown_png.hpp>
#include <cards/fishing_hut_png.hpp>
#include <cards/farm_png.hpp>
#include <cards/wood_camp_png.hpp>
#include <cards/stone_quarry_png.hpp>
#include <cards/temple_png.hpp>
#include <cards/hill_1_png.hpp>
#include <cards/hill_2_png.hpp>
#include <cards/hill_3_png.hpp>
#include <cards/water_1_png.hpp>
#include <cards/water_2_png.hpp>
#include <cards/water_3_png.hpp>

#include <icons/food_png.hpp>
#include <icons/wood_png.hpp>
#include <icons/stone_png.hpp>
#include <icons/prayer_png.hpp>

#include <sound/pop_mp3.hpp>
#include <sound/page_mp3.hpp>
#include <sound/error_mp3.hpp>
#include <sound/wave_mp3.hpp>
#include <sound/end_mp3.hpp>

#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace atlantis
{

	using namespace psemek;

	static geom::vector<int, 2> const forward_neighbours[3] =
	{
		{1, 0},
		{0, 1},
		{-1, 1},
	};

	static geom::vector<int, 2> neighbours[6] =
	{
		{1, 0},
		{0, 1},
		{-1, 1},
		{-1, 0},
		{0, -1},
		{1, -1},
	};

	static std::unordered_set<geom::vector<int, 2>> neighbours_2 = []{
		std::unordered_set<geom::vector<int, 2>> result;
		for (auto n : neighbours)
			for (auto n2 : neighbours)
				result.insert(n + n2);
		result.erase(geom::vector{0, 0});
		return result;
	}();

	enum class terrain_type
	{
		grassland,
		forest,
		stone,
		sand,
	};

	enum class resource_type
	{
		food,
		wood,
		stone,
		prayer,
	};

	gfx::color_rgba resource_color(resource_type type)
	{
		switch (type)
		{
		case resource_type::food: return {255, 0, 0, 255};
		case resource_type::wood: return {255, 191, 0, 255};
		case resource_type::stone: return {191, 191, 191, 255};
		case resource_type::prayer: return {127, 255, 255, 255};
		}

		return {255, 0, 255, 255};
	}

	enum class building_type
	{
		fishing_hut,
		farm,
		woodcutter,
		stone_quarry,
		temple,
	};

	psemek_declare_enum(card_type, std::uint32_t,
		(build_fishing_hut)
		(build_farm)
		(build_woodcutter)
		(build_stone_quarry)
		(build_temple)
		(freeze_1)
		(freeze_2)
		(freeze_3)
		(hill_1)
		(hill_2)
		(hill_3)
	)

	bool is_build_card(card_type type)
	{
		switch (type)
		{
		case card_type::build_fishing_hut:
		case card_type::build_farm:
		case card_type::build_woodcutter:
		case card_type::build_stone_quarry:
		case card_type::build_temple:
			return true;
		default:
			return false;
		}
	}

	bool is_freeze_card(card_type type)
	{
		switch (type)
		{
		case card_type::freeze_1:
		case card_type::freeze_2:
		case card_type::freeze_3:
			return true;
		default:
			return false;
		}
	}

	bool is_hill_card(card_type type)
	{
		switch (type)
		{
		case card_type::hill_1:
		case card_type::hill_2:
		case card_type::hill_3:
			return true;
		default:
			return false;
		}
	}

	std::unordered_map<resource_type, int> card_cost(card_type type)
	{
		switch (type)
		{
		case card_type::build_fishing_hut: return {{resource_type::wood, 50}};
		case card_type::build_farm: return {{resource_type::wood, 100}};
		case card_type::build_woodcutter: return {{resource_type::wood, 100}};
		case card_type::build_stone_quarry: return {{resource_type::wood, 200}};
		case card_type::build_temple: return {{resource_type::stone, 200}};
		case card_type::freeze_1: return {{resource_type::prayer, 100}};
		case card_type::freeze_2: return {{resource_type::prayer, 200}};
		case card_type::freeze_3: return {{resource_type::prayer, 300}};
		case card_type::hill_1: return {{resource_type::stone, 100}};
		case card_type::hill_2: return {{resource_type::stone, 200}};
		case card_type::hill_3: return {{resource_type::stone, 300}};
		}

		return {};
	}

	struct tile_data
	{
		terrain_type terrain;
		int elevation;
		std::optional<building_type> building;
	};

	using map_data = util::array<tile_data, 2>;

	static int const map_size = 40;

	static float const map_diagonal = map_size * std::sqrt(3.f);

	static float const tile_radius = 0.5f;
	static float const height_scale = 0.25f;

	static float const card_draw_period = 3.f;

	static float const turn_period = 1.f;

	static float const water_period = 60.f;

	static float const resource_particle_lifetime = 2.f;

	struct main_scene
		: app::scene_base
	{
		main_scene();

		void on_resize(int width, int height) override;

		void on_mouse_move(int x, int y, int dx, int dy) override;
		void on_mouse_wheel(int delta) override;

		void on_left_button_down() override;
		void on_right_button_down() override;

		void on_key_down(SDL_Keycode key) override;

		void update() override;
		void present() override;

	private:
		geom::spherical_camera camera_;
		float camera_azimuthal_angle_target_;
		float camera_elevation_angle_target_;
		float camera_distance_target_;

		audio::engine audio_engine_;
		std::shared_ptr<audio::track> pop_track_;
		std::shared_ptr<audio::track> page_track_;
		std::shared_ptr<audio::track> error_track_;
		std::shared_ptr<audio::track> wave_track_;
		std::shared_ptr<audio::track> end_track_;

		util::clock<std::chrono::duration<float>, std::chrono::high_resolution_clock> clock_;

		map_data map_;
		int water_level_ = -1;
		float water_height_ = - height_scale * 0.5f;

		float game_timer_ = 0.f;
		bool game_end_ = false;

		float turn_timer_ = 0.f;
		float water_timer_ = 0.f;
		float freeze_timer_ = 0.f;
		float escape_timer_ = 0.f;
		bool low_food_ = false;
		bool no_wood_ = false;

		std::optional<geom::point<int, 2>> selected_tile_;

		std::unordered_map<resource_type, int> resources_;
		std::optional<card_type> current_card_;
		std::vector<card_type> available_cards_;

		float card_draw_timer_ = 0.f;

		geom::box<float, 2> unknown_card_box_;
		geom::vector<float, 2> dropped_card_offset_{0.f, 0.f};

		geom::box<float, 2> current_card_box_;
		bool current_card_mouseover_ = false;
		geom::vector<float, 2> current_card_offset_{0.f, 0.f};
		bool applying_card_ = false;

		std::optional<card_type> discarded_card_;
		geom::vector<float, 2> discarded_card_offset_{0.f, 0.f};

		std::unordered_set<geom::point<int, 2>> highlighted_tiles_;

		struct resource_particle
		{
			std::unordered_map<resource_type, int> delta;
			geom::point<float, 3> position;

			float timer = 0.f;
		};

		std::vector<resource_particle> resource_particles_;

		random::generator rng_;
		util::array<float, 2> random_tile_rotation_;
		util::array<float, 2> random_tile_color_;
		util::array<std::array<geom::point<float, 3>, 7>, 2> tile_vertices_;

		gfx::program model_program_;
		gfx::program shadow_program_;

		gfx::mesh terrain_mesh_;
		gfx::mesh water_mesh_;

		gfx::mesh select_mesh_;
		gfx::mesh forest_mesh_;
		gfx::mesh forest_shadow_mesh_;
		gfx::mesh stone_mesh_;

		gfx::mesh fishing_hut_mesh_;
		gfx::mesh farm_mesh_;
		gfx::mesh woodcutter_mesh_;
		gfx::mesh woodcutter_shadow_mesh_;
		gfx::mesh stone_quarry_mesh_;
		gfx::mesh temple_mesh_;

		gfx::mesh connection_mesh_;

		gfx::texture_2d unknown_card_texture_;
		std::unordered_map<card_type, gfx::texture_2d> card_texture_;

		std::unordered_map<resource_type, gfx::texture_2d> resource_texture_;

		gfx::painter painter_;

		geom::vector<float, 2> tile_center(geom::point<int, 2> const & id);

		void update_terrain_mesh();
		void update_water_mesh();
		void update_hightlighted_tiles();
	};

	struct vertex
	{
		geom::point<float, 3> position;
		geom::vector<float, 3> normal;
		gfx::color_rgba color;

		static gfx::attribs_description attributes()
		{
			return gfx::make_attribs_description<geom::point<float, 3>, geom::vector<float, 3>, gfx::normalized<gfx::color_rgba>>();
		}
	};

	static char const model_vs[] =
R"(#version 330

uniform mat4 u_object_transform;
uniform mat4 u_camera_transform;

uniform vec3 u_light_direction;

layout (location = 0) in vec4 in_position;
layout (location = 1) in vec3 in_normal;
layout (location = 2) in vec4 in_color;

out vec3 position;
out vec4 color;

void main()
{
	vec4 world_position = u_object_transform * in_position;
	vec3 world_normal = mat3(u_object_transform) * in_normal;

	gl_Position = u_camera_transform * world_position;
	position = world_position.xyz;

	float lit = min(1.0, max(0.0, dot(world_normal, u_light_direction) + 0.5));

	color = vec4(in_color.rgb * lit, in_color.a);
}
)";

	static char const model_fs[] =
R"(#version 330

uniform vec3 u_camera_position;
uniform vec4 u_fog_color;
uniform vec4 u_color;
uniform float u_alpha;

in vec3 position;
in vec4 color;

layout (location = 0) out vec4 out_color;

void main()
{
	float camera_distance = length(position - u_camera_position);
	float fog_factor = 1.0 - exp(- camera_distance / 600.0);
	vec3 res_color = mix(mix(color.rgb, u_color.rgb, u_color.a), u_fog_color.rgb, fog_factor);
	res_color = pow(res_color, vec3(1.0 / 2.2));
	out_color = vec4(res_color, color.a * u_alpha);
}
)";

	static char const shadow_vs[] =
R"(#version 330

uniform mat4 u_object_transform;
uniform mat4 u_camera_transform;

layout (location = 0) in vec4 in_position;
layout (location = 2) in vec4 in_color;

out vec3 position;
out vec4 color;

void main()
{
	vec4 world_position = u_object_transform * in_position;
	gl_Position = u_camera_transform * world_position;
	position = world_position.xyz;

	color = vec4(0.0, 0.0, 0.0, in_color.r);
}
)";

	gfx::color_rgba ungamma(gfx::color_rgba const & c)
	{
		auto t = gfx::to_colorf(c);
		t[0] = std::pow(t[0], 2.2f);
		t[1] = std::pow(t[1], 2.2f);
		t[2] = std::pow(t[2], 2.2f);
		return gfx::to_coloru8(t);
	}

	main_scene::main_scene()
		: rng_{random::device{}}
		, model_program_(model_vs, model_fs)
		, shadow_program_(shadow_vs, model_fs)
	{
		camera_.near_clip = 1.f;
		camera_.far_clip = 1000.f;
		camera_.fov_y = geom::rad(45.f);
		camera_.fov_x = camera_.fov_y;

		camera_.distance = 40;
		camera_.elevation_angle = geom::rad(45.f);
		camera_.azimuthal_angle = geom::rad( 0.f);

		camera_azimuthal_angle_target_ = camera_.azimuthal_angle;
		camera_elevation_angle_target_ = camera_.elevation_angle;
		camera_distance_target_ = camera_.distance;

		pop_track_ = audio_engine_.load(sound::pop_mp3.data);
		page_track_ = audio_engine_.load(sound::page_mp3.data);
		error_track_ = audio_engine_.load(sound::error_mp3.data);
		wave_track_ = audio_engine_.load(sound::wave_mp3.data);
		end_track_ = audio_engine_.load(sound::end_mp3.data);

		map_.resize({map_size, map_size});

		auto map_center = tile_center({map_.width() / 2, map_.height() / 2});

		camera_.target = geom::point<float, 3>::zero() + geom::swizzle<0, 1, -1>(map_center);

		{
			random::uniform_sphere_vector_distribution<float, 2> d;

			util::array<geom::vector<float, 2>, 2> height_grad_map({map_size / 4, map_size / 4});
			for (auto & v : height_grad_map)
				v = d(rng_);
			pcg::perlin<float, 2> height_noise(std::move(height_grad_map));

			util::array<geom::vector<float, 2>, 2> terrain_grad_map_1({map_size / 2, map_size / 2});
			for (auto & v : terrain_grad_map_1)
				v = d(rng_);
			pcg::perlin<float, 2> terrain_noise_1(std::move(terrain_grad_map_1));

			util::array<geom::vector<float, 2>, 2> terrain_grad_map_2({map_size / 2, map_size / 2});
			for (auto & v : terrain_grad_map_2)
				v = d(rng_);
			pcg::perlin<float, 2> terrain_noise_2(std::move(terrain_grad_map_2));

			for (auto idx : map_.indices())
			{
				auto & tile = map_(idx);

				auto c = tile_center({idx[0], idx[1]});

				auto p = c / map_diagonal;

				float distance = std::min(1.f, geom::length(c - map_center) / map_diagonal * 2.f);

				float height = height_noise(p) * 10.f - distance * 20.f;

				tile.elevation = std::round(height);

				if (tile.elevation <= 0)
					tile.terrain = terrain_type::sand;
				else
				{
					auto t1 = terrain_noise_1(p);
					auto t2 = terrain_noise_2(p);

					if (t1 > 0.6f)
						tile.terrain = terrain_type::forest;
					else if (t2 > 0.7f)
						tile.terrain = terrain_type::stone;
					else
						tile.terrain = terrain_type::grassland;
				}
			}
		}

		random_tile_rotation_.resize(map_.dims());
		for (auto idx : map_.indices())
			random_tile_rotation_(idx) = geom::rad(random::uniform<int>(rng_, 0, 5) * 60.f + random::uniform<float>(rng_, -1.f, 1.f) * 15.f);

		random_tile_color_.resize(map_.dims());
		for (auto idx : map_.indices())
			random_tile_color_(idx) = random::uniform<float>(rng_, -1.f, 1.f) * 0.25f;

		update_terrain_mesh();
		update_water_mesh();

		{
			auto select = gfx::load_mesh(models::select.data);
			select_mesh_.setup(select.attribs);
			select_mesh_.load_raw(select);
		}

		{
			auto forest = gfx::load_mesh(models::forest.data);
			forest_mesh_.setup(forest.attribs);
			forest_mesh_.load_raw(forest);
		}

		{
			auto forest_shadow = gfx::load_mesh(models::forest_shadow.data);
			forest_shadow_mesh_.setup(forest_shadow.attribs);
			forest_shadow_mesh_.load_raw(forest_shadow);
		}

		{
			auto stone = gfx::load_mesh(models::stone.data);
			stone_mesh_.setup(stone.attribs);
			stone_mesh_.load_raw(stone);
		}

		{
			auto fishing_hut = gfx::load_mesh(models::fishing_hut.data);
			fishing_hut_mesh_.setup(fishing_hut.attribs);
			fishing_hut_mesh_.load_raw(fishing_hut);
		}

		{
			auto farm = gfx::load_mesh(models::farm.data);
			farm_mesh_.setup(farm.attribs);
			farm_mesh_.load_raw(farm);
		}

		{
			auto woodcutter = gfx::load_mesh(models::woodcutter.data);
			woodcutter_mesh_.setup(woodcutter.attribs);
			woodcutter_mesh_.load_raw(woodcutter);
		}

		{
			auto woodcutter_shadow = gfx::load_mesh(models::woodcutter_shadow.data);
			woodcutter_shadow_mesh_.setup(woodcutter_shadow.attribs);
			woodcutter_shadow_mesh_.load_raw(woodcutter_shadow);
		}

		{
			auto stone_quarry = gfx::load_mesh(models::stone_quarry.data);
			stone_quarry_mesh_.setup(stone_quarry.attribs);
			stone_quarry_mesh_.load_raw(stone_quarry);
		}

		{
			auto temple = gfx::load_mesh(models::temple.data);
			temple_mesh_.setup(temple.attribs);
			temple_mesh_.load_raw(temple);
		}

		unknown_card_texture_.load(gfx::read_png(io::memory_istream{cards::unknown_png.data}));
		unknown_card_texture_.nearest_filter();

		auto set_card_texture = [this](card_type type, rs::resource const & image)
		{
			card_texture_[type].load(gfx::read_png(io::memory_istream{image.data}));
			card_texture_[type].nearest_filter();
		};

		set_card_texture(card_type::build_fishing_hut, cards::fishing_hut_png);
		set_card_texture(card_type::build_farm, cards::farm_png);
		set_card_texture(card_type::build_woodcutter, cards::wood_camp_png);
		set_card_texture(card_type::build_stone_quarry, cards::stone_quarry_png);
		set_card_texture(card_type::build_temple, cards::temple_png);

		set_card_texture(card_type::freeze_1, cards::water_1_png);
		set_card_texture(card_type::freeze_2, cards::water_2_png);
		set_card_texture(card_type::freeze_3, cards::water_3_png);

		set_card_texture(card_type::hill_1, cards::hill_1_png);
		set_card_texture(card_type::hill_2, cards::hill_2_png);
		set_card_texture(card_type::hill_3, cards::hill_3_png);

		auto set_resource_texture = [this](resource_type type, rs::resource const & image)
		{
			resource_texture_[type].load(gfx::read_png(io::memory_istream{image.data}));
			resource_texture_[type].nearest_filter();
		};

		set_resource_texture(resource_type::food, icons::food_png);
		set_resource_texture(resource_type::wood, icons::wood_png);
		set_resource_texture(resource_type::stone, icons::stone_png);
		set_resource_texture(resource_type::prayer, icons::prayer_png);

		card_draw_timer_ = card_draw_period - 1.f;
		resources_[resource_type::wood] = 300;
		resources_[resource_type::food] = 200;
		resources_[resource_type::stone] = 200;
		resources_[resource_type::prayer] = 200;
	}

	void main_scene::on_resize(int width, int height)
	{
		app::scene_base::on_resize(width, height);
		camera_.set_fov(camera_.fov_y, width * 1.f / height);

		current_card_box_[0] = {width / 2.f + 25.f, width / 2.f + 175.f};
		current_card_box_[1] = {height - 150.f - 100.f, height - 150.f + 100.f};

		unknown_card_box_[0] = {width / 2.f - 175.f, width / 2.f - 25.f};
		unknown_card_box_[1] = {height - 150.f - 100.f, height - 150.f + 100.f};

		gl::Viewport(0, 0, width, height);
	}

	void main_scene::on_mouse_move(int x, int y, int dx, int dy)
	{
		app::scene_base::on_mouse_move(x, y, dx, dy);

		if (is_right_button_down() && !current_card_mouseover_)
		{
			camera_azimuthal_angle_target_ -= dx * 0.005f;
			camera_elevation_angle_target_ += dy * 0.005f;

			camera_elevation_angle_target_ = geom::clamp(camera_elevation_angle_target_, {geom::rad(30.f), geom::rad(75.f)});
		}
	}

	void main_scene::on_mouse_wheel(int delta)
	{
		app::scene_base::on_mouse_wheel(delta);

		camera_distance_target_ *= std::pow(0.8f, delta);

		camera_distance_target_ = geom::clamp(camera_distance_target_, {10.f, 40.f});
	}

	void main_scene::on_left_button_down()
	{
		app::scene_base::on_left_button_down();

		bool has_card_cost = true;

		if (current_card_)
		{
			auto cost = card_cost(*current_card_);
			for (auto const & p : cost)
			{
				if (resources_[p.first] < p.second)
				{
					has_card_cost = false;
					break;
				}
			}
		}

		if (current_card_ && current_card_mouseover_ && !applying_card_ && !has_card_cost)
			audio_engine_.play(error_track_);

		if (current_card_ && current_card_mouseover_ && !applying_card_ && has_card_cost)
		{
			if (is_freeze_card(*current_card_))
			{
				switch (*current_card_)
				{
				case card_type::freeze_1:
					freeze_timer_ += 10.f;
					break;
				case card_type::freeze_2:
					freeze_timer_ += 20.f;
					break;
				case card_type::freeze_3:
					freeze_timer_ += 30.f;
					break;
				default:
					break;
				}

				auto cost = card_cost(*current_card_);
				for (auto const & p : cost)
					resources_[p.first] -= p.second;

				audio_engine_.play(page_track_);

				discarded_card_offset_ = {0.f, 0.f};
				discarded_card_ = current_card_;
				current_card_ = std::nullopt;
				current_card_mouseover_ = false;
			}
			else
			{
				applying_card_ = true;
				update_hightlighted_tiles();
			}
		}
		else if (current_card_ && applying_card_ && selected_tile_)
		{
			if (highlighted_tiles_.contains(*selected_tile_))
			{
				audio_engine_.play(pop_track_);

				auto p = *selected_tile_;

				auto & tile = map_(p[0], p[1]);

				auto cost = card_cost(*current_card_);
				for (auto const & p : cost)
					resources_[p.first] -= p.second;

				resource_particle part;
				for (auto const & p : cost)
					part.delta[p.first] = -p.second;
				part.position[0] = tile_center(*selected_tile_)[0];
				part.position[1] = tile_center(*selected_tile_)[1];
				part.position[2] = (tile.elevation + 1.f) * height_scale;
				resource_particles_.push_back(part);

				switch (*current_card_)
				{
				case card_type::build_farm:
					tile.building = building_type::farm;
					tile.terrain = terrain_type::grassland;
					update_terrain_mesh();
					break;
				case card_type::build_fishing_hut:
					tile.building = building_type::fishing_hut;
					tile.terrain = terrain_type::sand;
					update_terrain_mesh();
					break;
				case card_type::build_woodcutter:
					tile.building = building_type::woodcutter;
					break;
				case card_type::build_stone_quarry:
					tile.building = building_type::stone_quarry;
					break;
				case card_type::build_temple:
					tile.building = building_type::temple;
					tile.terrain = terrain_type::grassland;
					update_terrain_mesh();
					break;
				case card_type::hill_1:
					tile.elevation += 1;
					update_terrain_mesh();
					break;
				case card_type::hill_2:
					tile.elevation += 1;
					for (auto n : neighbours)
					{
						auto q = p + n;
						if (q[0] < 0 || q[1] < 0 || q[0] >= map_.width() || q[1] >= map_.height()) continue;
						int & e = map_(q[0], q[1]).elevation;
						if (e <= water_level_) continue;
						e += 1;
					}
					update_terrain_mesh();
					break;
				case card_type::hill_3:
					tile.elevation += 1;
					for (auto n : neighbours_2)
					{
						auto q = p + n;
						if (q[0] < 0 || q[1] < 0 || q[0] >= map_.width() || q[1] >= map_.height()) continue;
						int & e = map_(q[0], q[1]).elevation;
						if (e <= water_level_) continue;
						e += 1;
					}
					update_terrain_mesh();
					break;
				default:
					break;
				}

				current_card_ = std::nullopt;
				applying_card_ = false;
				highlighted_tiles_.clear();
			}
		}
	}

	void main_scene::on_right_button_down()
	{
		app::scene_base::on_right_button_down();

		if (current_card_ && applying_card_)
		{
			applying_card_ = false;
			highlighted_tiles_.clear();
		}
		else if (current_card_ && current_card_mouseover_)
		{
			audio_engine_.play(page_track_);
			discarded_card_ = current_card_;
			discarded_card_offset_ = {0.f, 0.f};
			current_card_ = std::nullopt;
			current_card_mouseover_ = false;
			freeze_timer_ += 1.f;
		}
	}

	void main_scene::on_key_down(SDL_Keycode key)
	{
		if (key == SDLK_ESCAPE)
		{
			if (escape_timer_ > 0.f || game_end_)
				parent()->stop();
			else
				escape_timer_ += 3.f;
		}

		if (key == SDLK_r)
		{
			auto app = parent();
			auto self = app->pop_scene();
			app->push_scene(std::make_shared<main_scene>());
		}
	}

	void main_scene::update()
	{
		float const dt = clock_.restart().count();

		if (!game_end_)
			game_timer_ += dt;

		if (escape_timer_ > 0.f)
			escape_timer_ -= dt;

		camera_.azimuthal_angle += (camera_azimuthal_angle_target_ - camera_.azimuthal_angle) * (1.f - std::exp(- 20.f * dt));
		camera_.elevation_angle += (camera_elevation_angle_target_ - camera_.elevation_angle) * (1.f - std::exp(- 20.f * dt));
		camera_.distance += (camera_distance_target_ - camera_.distance) * (1.f - std::exp(- 10.f * dt));

		card_draw_timer_ += dt;
		if (!game_end_ && card_draw_timer_ >= card_draw_period)
		{
			if (available_cards_.empty())
			{
				for (int repeats = 0; repeats < 1; ++repeats)
				{
					available_cards_.push_back(card_type::build_farm);
					available_cards_.push_back(card_type::build_fishing_hut);

					available_cards_.push_back(card_type::build_woodcutter);
					available_cards_.push_back(card_type::build_stone_quarry);
					available_cards_.push_back(card_type::build_temple);

					if (int k = random::uniform<int>(rng_, 0, 2); k == 0)
						available_cards_.push_back(card_type::freeze_1);
					else if (k == 1)
						available_cards_.push_back(card_type::freeze_2);
					else
						available_cards_.push_back(card_type::freeze_3);

					if (int k = random::uniform<int>(rng_, 0, 2); k == 0)
						available_cards_.push_back(card_type::hill_1);
					else if (k == 1)
						available_cards_.push_back(card_type::hill_2);
					else
						available_cards_.push_back(card_type::hill_3);
				}

				std::random_shuffle(available_cards_.begin(), available_cards_.end(), [this](auto n){ return random::uniform<int>(rng_, 0, n - 1); });
			}

			audio_engine_.play(page_track_);

			if (current_card_)
			{
				available_cards_.pop_back();
				dropped_card_offset_ = {0.f, 0.f};
			}
			else
			{
				current_card_ = available_cards_.back();
				available_cards_.pop_back();
				current_card_offset_ = {-200.f, 0.f};
			}

			card_draw_timer_ -= card_draw_period;
		}

		turn_timer_ += dt;
		if (!game_end_ && turn_timer_ >= turn_period)
		{
			turn_timer_ -= turn_period;

			low_food_ = false;
			no_wood_ = true;
			for (auto idx : map_.indices())
			{
				auto const & tile = map_(idx);

				geom::point<int, 2> p{idx[0], idx[1]};

				float multiplier = 1.f;

				for (auto n : neighbours)
				{
					auto q = p + n;
					if (q[0] < 0 || q[1] < 0 || q[0] >= map_.width() || q[1] >= map_.height()) continue;

					auto const & qtile = map_(q[0], q[1]);

					if (qtile.building && qtile.elevation != tile.elevation)
						multiplier += 0.5f;
				}

				std::unordered_map<resource_type, int> resource_delta;

				if (tile.building)
				{
					switch (*tile.building)
					{
					case building_type::fishing_hut:
						resource_delta[resource_type::food] = 8 * multiplier;
						break;
					case building_type::farm:
						resource_delta[resource_type::food] = 4 * multiplier;
						break;
					case building_type::woodcutter:
						resource_delta[resource_type::wood] = 2 * multiplier;
						resource_delta[resource_type::food] = -5;
						no_wood_ = false;
						break;
					case building_type::stone_quarry:
						resource_delta[resource_type::stone] = 2 * multiplier;
						resource_delta[resource_type::food] = -5;
						break;
					case building_type::temple:
						resource_delta[resource_type::prayer] = 2 * multiplier;
						resource_delta[resource_type::food] = -5;
						break;
					}
				}

				bool can_apply = true;
				for (auto const & p : resource_delta)
					if (resources_[p.first] + p.second < 0)
						can_apply = false;

				if (can_apply)
				{
					for (auto const & p : resource_delta)
						resources_[p.first] += p.second;

					resource_particle part;
					part.position[0] = tile_center({idx[0], idx[1]})[0];
					part.position[1] = tile_center({idx[0], idx[1]})[1];
					part.position[2] = (tile.elevation + 1.f) * height_scale;
					part.delta = resource_delta;

					resource_particles_.push_back(part);
				}
				else
					low_food_ = true;
			}

			no_wood_ &= resources_[resource_type::wood] < card_cost(card_type::build_woodcutter)[resource_type::wood];
		}

		if (freeze_timer_ > 0.f)
			freeze_timer_ -= dt;
		else
			water_timer_ += dt;

		if (!game_end_ && water_timer_ >= water_period)
		{
			water_timer_ -= water_period;
			water_level_ += 1;

			audio_engine_.play(wave_track_);

			game_end_ = true;
			for (auto & tile : map_)
			{
				if (tile.elevation <= water_level_)
				{
					tile.terrain = terrain_type::sand;
					tile.building = std::nullopt;
				}
				else
					game_end_ = false;
			}

			if (game_end_)
				audio_engine_.play(end_track_);

			update_terrain_mesh();
			update_hightlighted_tiles();
		}

		float water_height_target = (water_level_ + 0.5f) * height_scale;
		water_height_ += (water_height_target - water_height_) * (1.f - std::exp(- 1.f * dt));
		update_water_mesh();

		camera_.target[2] = water_height_;

		current_card_mouseover_ = false;
		if (!game_end_ && current_card_ && mouse() && geom::contains(current_card_box_, geom::cast<float>(*mouse())))
			current_card_mouseover_ = true;

		geom::vector<float, 2> current_card_offset_target{0.f, 0.f};
		if (applying_card_ && mouse())
			current_card_offset_target = geom::cast<float>(*mouse()) - current_card_box_.corner(0.f, 0.f) + geom::vector{5.f, 5.f};
		else if (current_card_mouseover_)
			current_card_offset_target = {15.f, -15.f};

		current_card_offset_ += (current_card_offset_target - current_card_offset_) * (1.f - std::exp(- 20.f * dt));

		if (discarded_card_)
			discarded_card_offset_[1] += dt * 2000.f;

		dropped_card_offset_[1] += dt * 2000.f;

		selected_tile_ = std::nullopt;
		if (mouse())
		{
			geom::ray<float, 3> mouse_ray;
			mouse_ray.origin = camera_.position();
			mouse_ray.direction = camera_.direction((*mouse())[0] * 2.f / width() - 1.f, 1.f - (*mouse())[1] * 2.f / height());

			float tmin = std::numeric_limits<float>::infinity();

			for (auto idx : map_.indices())
			{
				auto const & vertices = tile_vertices_(idx);
				for (int i = 0; i < 6; ++i)
				{
					geom::triangle<geom::point<float, 3>> triangle;
					triangle[0] = vertices[0];
					triangle[1] = vertices[i + 1];
					triangle[2] = vertices[((i + 1) % 6) + 1];

					if (auto t = geom::intersection(mouse_ray, triangle))
					{
						if (*t < tmin)
						{
							tmin = *t;
							selected_tile_ = {idx[0], idx[1]};
						}
					}
				}
			}
		}

		std::vector<resource_particle> alive_resource_particles;
		for (auto & p : resource_particles_)
		{
			p.position[2] += dt * 0.5f;

			p.timer += dt;
			if (p.timer < resource_particle_lifetime)
				alive_resource_particles.push_back(p);
		}
		resource_particles_ = std::move(alive_resource_particles);
	}

	void main_scene::present()
	{
		gl::DepthMask(gl::TRUE);
		gl::ClearColor(0.8f, 0.8f, 1.f, 1.f);
		gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

		gl::Enable(gl::CULL_FACE);

		gl::Enable(gl::DEPTH_TEST);
		gl::DepthFunc(gl::LESS);

		gl::Disable(gl::BLEND);

		auto light = geom::normalized(geom::vector{1.f, 1.f, 1.f});;

		model_program_.bind();
		model_program_["u_object_transform"] = geom::matrix<float, 4, 4>::identity();
		model_program_["u_camera_transform"] = camera_.transform();
		model_program_["u_light_direction"] = light;
		model_program_["u_camera_position"] = camera_.position();
		model_program_["u_fog_color"] = geom::vector{0.8f, 0.8f, 1.f, 1.f};
		model_program_["u_color"] = geom::vector{0.f, 0.f, 0.f, 0.f};
		model_program_["u_alpha"] = 1.f;

		terrain_mesh_.draw();
		water_mesh_.draw();

		std::vector<geom::segment<geom::point<float, 3>>> connections;

		for (auto idx : map_.indices())
		{
			auto const & tile = map_(idx);

			if ((tile.terrain == terrain_type::forest || tile.terrain == terrain_type::stone) && !tile.building)
			{
				auto c = tile_center({idx[0], idx[1]});
				float h = height_scale * tile.elevation;
				model_program_["u_object_transform"] = geom::translation<float, 3>({c[0], c[1], h}).homogeneous_matrix()
					* geom::scale<float, 3>(tile_radius).homogeneous_matrix()
					* geom::plane_rotation<float, 3>(0, 1, random_tile_rotation_(idx)).homogeneous_matrix();

				if (tile.terrain == terrain_type::forest)
				{
					model_program_["u_color"] = geom::vector{0.f, 0.f, 0.f, 0.5f};
					forest_mesh_.draw();
				}
				else if (tile.terrain == terrain_type::stone)
				{
					model_program_["u_color"] = geom::vector{0.f, 0.f, 0.f, 0.f};
					stone_mesh_.draw();
				}
			}

			auto tile_connection_vertex = [&](geom::point<int, 2> const & p)
			{
				geom::point<float, 3> v;
				v[0] = tile_center(p)[0];
				v[1] = tile_center(p)[1];
				v[2] = (map_(p[0], p[1]).elevation + 1.f) * height_scale;
				return v;
			};

			if (tile.building)
			{
				geom::point<int, 2> p{idx[0], idx[1]};
				auto c = tile_center(p);
				float h = height_scale * map_(idx).elevation;
				model_program_["u_object_transform"] = geom::translation<float, 3>({c[0], c[1], h}).homogeneous_matrix()
					* geom::scale<float, 3>(tile_radius).homogeneous_matrix()
					* geom::plane_rotation<float, 3>(0, 1, random_tile_rotation_(idx)).homogeneous_matrix();

				model_program_["u_color"] = geom::vector{0.f, 0.f, 0.f, 0.f};
				switch (*tile.building)
				{
				case building_type::fishing_hut:
					fishing_hut_mesh_.draw();
					break;
				case building_type::farm:
					farm_mesh_.draw();
					break;
				case building_type::woodcutter:
					woodcutter_mesh_.draw();
					break;
				case building_type::stone_quarry:
					stone_quarry_mesh_.draw();
					break;
				case building_type::temple:
					temple_mesh_.draw();
					break;
				}

				for (auto n : forward_neighbours)
				{
					auto q = p + n;
					if (q[0] < 0 || q[1] < 0 || q[0] >= map_.width() || q[1] >= map_.height()) continue;

					auto const & qtile = map_(q[0], q[1]);

					if (qtile.building && qtile.elevation != tile.elevation)
					{
						connections.push_back({tile_connection_vertex(p), tile_connection_vertex(q)});
					}
				}
			}
		}

		gl::Enable(gl::BLEND);
		gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
		gl::DepthMask(gl::FALSE);

		model_program_["u_color"] = geom::vector{0.f, 0.f, 1.f, 1.f};
		model_program_["u_alpha"] = 0.75f;
		for (auto p : highlighted_tiles_)
		{
			std::array<std::size_t, 2> idx{p[0], p[1]};
			auto c = tile_center(p);
			float h = height_scale * map_(idx).elevation;
			model_program_["u_object_transform"] = geom::translation<float, 3>({c[0], c[1], h}).homogeneous_matrix()
				* geom::scale<float, 3>(tile_radius).homogeneous_matrix();

			select_mesh_.draw();
		}

		if (selected_tile_ && highlighted_tiles_.contains(*selected_tile_))
		{
			std::vector<geom::point<int, 2>> shown_tiles;
			shown_tiles.push_back(*selected_tile_);

			if (current_card_)
			{
				switch (*current_card_)
				{
				case card_type::hill_2:
					for (auto n : neighbours)
						shown_tiles.push_back(*selected_tile_ + n);
					break;
				case card_type::hill_3:
					for (auto n : neighbours_2)
						shown_tiles.push_back(*selected_tile_ + n);
					break;
				default:
					break;
				}
			}

			model_program_["u_color"] = geom::vector{1.f, 1.f, 1.f, 1.f};
			model_program_["u_alpha"] = 1.f;
			for (auto p : shown_tiles)
			{
				std::array<std::size_t, 2> idx{p[0], p[1]};
				auto c = tile_center({idx[0], idx[1]});
				float h = height_scale * map_(idx).elevation;
				model_program_["u_object_transform"] = geom::translation<float, 3>({c[0], c[1], h}).homogeneous_matrix()
					* geom::scale<float, 3>(tile_radius).homogeneous_matrix();

				select_mesh_.draw();
			}
		}

		shadow_program_.bind();
		shadow_program_["u_object_transform"] = geom::matrix<float, 4, 4>::identity();
		shadow_program_["u_camera_transform"] = camera_.transform();
		shadow_program_["u_light_direction"] = light;
		shadow_program_["u_camera_position"] = camera_.position();
		shadow_program_["u_fog_color"] = geom::vector{0.8f, 0.8f, 1.f, 1.f};
		shadow_program_["u_color"] = geom::vector{0.f, 0.f, 0.f, 0.f};
		shadow_program_["u_alpha"] = 1.f;

		for (auto idx : map_.indices())
		{
			if (map_(idx).terrain == terrain_type::forest && !map_(idx).building)
			{
				auto c = tile_center({idx[0], idx[1]});
				float h = height_scale * map_(idx).elevation;
				shadow_program_["u_object_transform"] = geom::translation<float, 3>({c[0], c[1], h}).homogeneous_matrix()
					* geom::scale<float, 3>(tile_radius).homogeneous_matrix()
					* geom::plane_rotation<float, 3>(0, 1, random_tile_rotation_(idx)).homogeneous_matrix();

				if (map_(idx).terrain == terrain_type::forest)
				{
					shadow_program_["u_color"] = geom::vector{0.f, 0.f, 0.f, 1.f};
					forest_shadow_mesh_.draw();
				}
			}
		}

		for (auto & s : connections)
		{
			auto x = s[1] - s[0];
			x *= 0.3f;
			s[0] += x;
			s[1] -= x;
		}

		{
			model_program_.bind();

			connection_mesh_.setup(vertex::attributes());

			auto n = light;

			std::vector<vertex> connection_vertices;

			float const radius = 1.f / 32.f;

			gfx::color_rgba c0{255, 0, 255, 0};
			gfx::color_rgba c1{255, 255, 255, 255};

			for (auto const & s : connections)
			{
				auto x = geom::normalized(s[1] - s[0]) * radius;
				auto y = geom::normalized(geom::ort(camera_.axis_z(), s[1] - s[0])) * radius;

				connection_vertices.push_back({s[0]    , n, c1});
				connection_vertices.push_back({s[1]    , n, c1});
				connection_vertices.push_back({s[1] + y, n, c0});

				connection_vertices.push_back({s[0]    , n, c1});
				connection_vertices.push_back({s[1] + y, n, c0});
				connection_vertices.push_back({s[0] + y, n, c0});

				connection_vertices.push_back({s[0] - y, n, c0});
				connection_vertices.push_back({s[1] - y, n, c0});
				connection_vertices.push_back({s[1]    , n, c1});

				connection_vertices.push_back({s[0] - y, n, c0});
				connection_vertices.push_back({s[1]    , n, c1});
				connection_vertices.push_back({s[0]    , n, c1});

				int N = 6;

				for (int i = 0; i < N; ++i)
				{
					float a0 = (i * geom::pi) / N;
					float a1 = ((i + 1) * geom::pi) / N;

					connection_vertices.push_back({s[1], n, c1});
					connection_vertices.push_back({s[1] - y * std::cos(a0) + x * std::sin(a0), n, c0});
					connection_vertices.push_back({s[1] - y * std::cos(a1) + x * std::sin(a1), n, c0});

					connection_vertices.push_back({s[0], n, c1});
					connection_vertices.push_back({s[0] + y * std::cos(a0) - x * std::sin(a0), n, c0});
					connection_vertices.push_back({s[0] + y * std::cos(a1) - x * std::sin(a1), n, c0});
				}
			}

			model_program_["u_object_transform"] = geom::matrix<float, 4, 4>::identity();
			model_program_["u_color"] = geom::vector{0.f, 0.f, 0.f, 0.f};
			model_program_["u_alpha"] = 1.f;

			connection_mesh_.load(connection_vertices, gl::TRIANGLES, gl::STREAM_DRAW);
			connection_mesh_.draw();
		}

		auto put_text = [this](auto & opts, geom::point<float, 2> const & p, auto const & string)
		{
			auto color = opts.c;
			opts.c = {0, 0, 0, color[3]};
			painter_.text(p + geom::vector{1.f, 1.f}, string, opts);

			opts.c = color;
			painter_.text(p, string, opts);
		};

		{
			gfx::painter::text_options opts;
			opts.scale = 2.f;
			opts.x = gfx::painter::x_align::left;
			opts.y = gfx::painter::y_align::center;

			opts.c = resource_color(resource_type::food);
			painter_.texture(resource_texture_.at(resource_type::food), {{{10.f, 50.f}, {10.f, 50.f}}});
			put_text(opts, {60.f, 30.f}, util::to_string("Food: ", resources_[resource_type::food]));

			opts.c = resource_color(resource_type::wood);
			painter_.texture(resource_texture_.at(resource_type::wood), {{{10.f, 50.f}, {60.f, 100.f}}});
			put_text(opts, {60.f, 80.f}, util::to_string("Wood: ", resources_[resource_type::wood]));

			opts.c = resource_color(resource_type::stone);
			painter_.texture(resource_texture_.at(resource_type::stone), {{{10.f, 50.f}, {110.f, 150.f}}});
			put_text(opts, {60.f, 130.f}, util::to_string("Stone: ", resources_[resource_type::stone]));

			opts.c = resource_color(resource_type::prayer);
			painter_.texture(resource_texture_.at(resource_type::prayer), {{{10.f, 50.f}, {160.f, 200.f}}});
			put_text(opts, {60.f, 180.f}, util::to_string("Prayers: ", resources_[resource_type::prayer]));

			float y = 230.f;

			if (low_food_)
			{
				opts.c = resource_color(resource_type::food);
				put_text(opts, {60.f, y}, util::to_string("Low food! Some buildings"));
				put_text(opts, {60.f, y + 25.f}, util::to_string("  will not function properly"));

				y += 75.f;
			}

			if (no_wood_)
			{
				opts.c = {255, 255, 255, 255};
				put_text(opts, {60.f, y}, util::to_string("Low wood and no wood camps built!"));

				y += 50.f;
			}
		}

		auto draw_wheel = [&](float value, geom::point<float, 2> const & center, float inner_radius, float outer_radius, gfx::color_rgba const & color)
		{
			int N = 72;
			for (int i = 0; i < N; ++i)
			{
				if (value < (i * 1.f) / N) break;

				float a0 = (2.f * i * geom::pi) / N;
				float a1 = (2.f * (i + 1) * geom::pi) / N;

				if (value < (i + 1.f) / N)
					a1 = 2.f * value * geom::pi;

				geom::vector<float, 2> v0{std::sin(a0), -std::cos(a0)};
				geom::vector<float, 2> v1{std::sin(a1), -std::cos(a1)};

				auto i0 = center + v0 * inner_radius;
				auto i1 = center + v1 * inner_radius;
				auto o0 = center + v0 * outer_radius;
				auto o1 = center + v1 * outer_radius;

				painter_.triangle(i0, i1, o0, color);
				painter_.triangle(o0, i1, o1, color);
			}
		};

		if (!game_end_)
		{
			gfx::painter::text_options opts;
			opts.scale = 2.f;
			opts.x = gfx::painter::x_align::center;
			opts.y = gfx::painter::y_align::center;
			opts.c = {255, 255, 255, 255};

			float t = card_draw_period - card_draw_timer_;
			int s = std::ceil(t);

			draw_wheel(card_draw_timer_ / card_draw_period, {unknown_card_box_[0].min - 75.f, unknown_card_box_[1].center()}, 38.f, 52.f, {0, 0, 0, 255});
			draw_wheel(card_draw_timer_ / card_draw_period, {unknown_card_box_[0].min - 75.f, unknown_card_box_[1].center()}, 40.f, 50.f, {127, 0, 0, 255});
			put_text(opts, {unknown_card_box_[0].min - 75.f, unknown_card_box_[1].center()}, util::to_string(s, "s"));

			opts.x = gfx::painter::x_align::right;
			put_text(opts, {unknown_card_box_[0].min - 150.f, unknown_card_box_[1].center()}, util::to_string("New card in"));
		}

		if (!game_end_)
		{
			gfx::painter::text_options opts;
			opts.scale = 2.f;
			opts.x = gfx::painter::x_align::center;
			opts.y = gfx::painter::y_align::center;
			opts.c = {255, 255, 255, 255};

			float t = water_period - water_timer_;
			int s = std::ceil(t);

			draw_wheel(1.f - water_timer_ / water_period, {width() / 2.f, 75.f}, 38.f, 52.f, {0, 0, 0, 255});
			draw_wheel(1.f - water_timer_ / water_period, {width() / 2.f, 75.f}, 40.f, 50.f, {0, (freeze_timer_ > 0.f ? 255 : 0), 255, 255});
			put_text(opts, {width() / 2.f, 75.f}, util::to_string(s, "s"));

			opts.x = gfx::painter::x_align::right;
			put_text(opts, {width() / 2.f - 75.f, 75.f}, util::to_string("Water rise in"));

			if (freeze_timer_ > 0.f)
			{
				float ft = freeze_timer_;
				int fs = std::ceil(ft);

				opts.c = {0, 255, 255, 255};
				put_text(opts, {width() / 2.f - 75.f, 100.f}, util::to_string("(freezed for ", fs, "s)"));
			}
		}

		{
			gfx::painter::text_options opts;
			opts.scale = 1.f;
			opts.x = gfx::painter::x_align::right;
			opts.y = gfx::painter::y_align::center;

			auto transform = camera_.transform();

			for (auto const & p : resource_particles_)
			{
				auto w = transform * geom::homogeneous(p.position);

				int i = 0;

				for (auto const & q : p.delta)
				{
					opts.c = resource_color(q.first);
					opts.c[3] = 255 * (1.f - p.timer / resource_particle_lifetime);

					float x = (0.5f * w[0] / w[3] + 0.5f) * width();
					float y = (0.5f - 0.5f * w[1] / w[3]) * height() + (i - p.delta.size() / 2.f) * 20.f;

					painter_.texture(resource_texture_.at(q.first), {{{x + 5.f, x + 25.f}, {y - 10.f, y + 10.f}}});
					put_text(opts, {x, y}, util::to_string(q.second > 0 ? '+' : '-', std::abs(q.second)));

					++i;
				}
			}
		}

		if (!game_end_)
		{
			painter_.texture(unknown_card_texture_, unknown_card_box_);
			painter_.texture(unknown_card_texture_, unknown_card_box_ + dropped_card_offset_);
		}

		if (!game_end_ && current_card_)
		{
			auto box = current_card_box_ + current_card_offset_;
			if (applying_card_)
				box = geom::shrink(box, box.dimensions() * 0.25f);

			if (card_texture_.contains(*current_card_))
				painter_.texture(card_texture_.at(*current_card_), box);

			std::optional<resource_type> not_enough;
			auto cost = card_cost(*current_card_);
			for (auto const & p : cost)
				if (resources_[p.first] < p.second)
					not_enough = p.first;

			if (current_card_mouseover_ && !applying_card_)
			{
				gfx::painter::text_options opts;
				opts.scale = 2.f;
				opts.x = gfx::painter::x_align::left;
				opts.y = gfx::painter::y_align::center;

				opts.c = {255, 255, 255, 255};
				if (!not_enough)
					put_text(opts, {current_card_box_[0].max + 50.f, current_card_box_[1].center() - 25.f}, "Left click to use");
				put_text(opts, {current_card_box_[0].max + 50.f, current_card_box_[1].center() + 25.f}, "Right click to discard for 1s freeze");

				if (not_enough)
				{
					std::string str;
					switch (*not_enough)
					{
					case resource_type::food:
						str = "food";
						break;
					case resource_type::wood:
						str = "wood";
						break;
					case resource_type::stone:
						str = "stone";
						break;
					case resource_type::prayer:
						str = "prayers";
						break;
					}

					opts.c = resource_color(*not_enough);
					put_text(opts, {current_card_box_[0].max + 50.f, current_card_box_[1].center() - 25.f},  util::to_string("Not enough ", str));
				}

				if (is_build_card(*current_card_))
				{
					opts.c = {255, 255, 255, 255};
					opts.x = gfx::painter::x_align::center;
					put_text(opts, {current_card_box_[0].center(), current_card_box_[1].min - 100.f}, "Get +50% efficiency for each pair");
					put_text(opts, {current_card_box_[0].center(), current_card_box_[1].min - 75.f}, "of buildings on neighbouring tiles");
					put_text(opts, {current_card_box_[0].center(), current_card_box_[1].min - 50.f}, "with different height");
				}
			}
		}

		if (!game_end_ && discarded_card_)
			if (card_texture_.contains(*discarded_card_))
				painter_.texture(card_texture_.at(*discarded_card_), current_card_box_ + discarded_card_offset_);

		if (!game_end_ && escape_timer_ > 0.f)
		{
			gfx::painter::text_options opts;
			opts.scale = 2.f;
			opts.x = gfx::painter::x_align::right;
			opts.y = gfx::painter::y_align::top;

			opts.c = {255, 255, 255, 255};
			put_text(opts, {width() - 10.f, 10.f}, "Press [ESC] again to exit");
			put_text(opts, {width() - 10.f, 35.f}, "Press [R] to restart     ");
		}

		{
			gfx::painter::text_options opts;
			opts.scale = 1.f;
			opts.x = gfx::painter::x_align::right;
			opts.y = gfx::painter::y_align::bottom;

			opts.c = {255, 255, 0, 255};
			put_text(opts, {width() - 10.f, height() - 24.f}, "   Made by lisyarus (lisyarus.itch.io)");
			put_text(opts, {width() - 10.f, height() - 10.f}, "for Ludum Dare 50 Compo (3 April 2022)");
		}

		if (game_end_)
		{
			gfx::painter::text_options opts;
			opts.scale = 2.f;
			opts.x = gfx::painter::x_align::center;
			opts.y = gfx::painter::y_align::center;

			float t = game_timer_;
			int m = std::floor(t / 60.f);
			int s = std::floor(t - m * 60.f);

			opts.c = {255, 255, 0, 255};
			if (m == 0)
				put_text(opts, {width() / 2.f, height() / 2.f - 25.f}, util::to_string("You lasted ", s, " seconds!"));
			else
				put_text(opts, {width() / 2.f, height() / 2.f - 25.f}, util::to_string("You lasted ", m, " minutes ", s, " seconds!"));

			opts.c = {255, 255, 255, 255};
			put_text(opts, {width() / 2.f, height() / 2.f},        util::to_string("Press [ESC] to exit "));
			put_text(opts, {width() / 2.f, height() / 2.f + 25.f}, util::to_string("Press [R] to restart"));
		}

		painter_.render(geom::window_camera{width(), height()}.transform());
	}

	geom::vector<float, 2> main_scene::tile_center(geom::point<int, 2> const & id)
	{
		static geom::vector<float, 2> const a{1.f, 0.f};
		static geom::vector<float, 2> const b = geom::rotate(a, geom::rad(60.f));
		return static_cast<float>(id[0]) * a + static_cast<float>(id[1]) * b;
	}

	void main_scene::update_terrain_mesh()
	{
		tile_vertices_.resize(map_.dims());

		geom::vector<float, 2> tile_corner_offset[6];
		for (int i = 0; i < 6; ++i)
			tile_corner_offset[i] = geom::rotate(geom::vector{1.f, 0.f}, geom::rad(30.f + i * 60.f)) * tile_radius;

		util::array<geom::point<float, 3>, 2> tile_centers(map_.dims());
		util::array<std::array<geom::point<float, 3>, 6>, 2> tile_corners(map_.dims());
		util::array<gfx::color_rgba, 2> tile_colors(map_.dims());

		for (auto idx : map_.indices())
		{
			auto center = tile_center({idx[0], idx[1]});
			float height = height_scale * map_(idx).elevation;

			tile_centers(idx) = {center[0], center[1], height};
			for (int i = 0; i < 6; ++i)
				tile_corners(idx)[i] = tile_centers(idx) + geom::swizzle<0, 1, -1>(tile_corner_offset[i]);

			tile_vertices_(idx)[0] = tile_centers(idx);
			for (int i = 0; i < 6; ++i)
				tile_vertices_(idx)[i + 1] = tile_corners(idx)[i];

			switch (map_(idx).terrain)
			{
			case terrain_type::grassland:
				tile_colors(idx) = {31, 127, 0, 255};
				break;
			case terrain_type::forest:
				tile_colors(idx) = {63, 31, 0, 255};
				break;
			case terrain_type::stone:
				tile_colors(idx) = {127, 127, 127, 255};
				break;
			case terrain_type::sand:
				tile_colors(idx) = {255, 255, 191, 255};
				break;
			}

			float t = random_tile_color_(idx);
			if (t < 0.f)
				tile_colors(idx) = gfx::dark(tile_colors(idx), -t);
			else
				tile_colors(idx) = gfx::light(tile_colors(idx), t);

			tile_colors(idx) = ungamma(tile_colors(idx));
		}

		std::vector<geom::triangle<vertex>> triangles;

		geom::vector n{0.f, 0.f, 1.f};

		for (auto idx : map_.indices())
		{
			for (int i = 0; i < 6; ++i)
			{
				int j = (i + 1) % 6;

				auto v0 = tile_centers(idx);
				auto v1 = tile_corners(idx)[i];
				auto v2 = tile_corners(idx)[j];

				gfx::color_rgba c = tile_colors(idx);

				triangles.push_back({{ {v0, n, c}, {v1, n, c}, {v2, n, c} }});
			}
		}

		for (auto idx : map_.indices())
		{
			if (idx[0] + 1 < map_.width() && idx[1] + 1 < map_.height())
			{
				auto v0 = tile_corners(idx)[0];
				auto v1 = tile_corners({idx[0] + 1, idx[1]})[2];
				auto v2 = tile_corners({idx[0], idx[1] + 1})[4];

				auto c0 = tile_colors(idx);
				auto c1 = tile_colors({idx[0] + 1, idx[1]});
				auto c2 = tile_colors({idx[0], idx[1] + 1});

				c0 = gfx::dark(c0, 0.5f);
				c1 = gfx::dark(c1, 0.5f);
				c2 = gfx::dark(c2, 0.5f);

				triangles.push_back({{ {v0, n, c0}, {v1, n, c1}, {v2, n, c2} }});
			}

			if (idx[0] > 0 && idx[1] > 0)
			{
				auto v0 = tile_corners(idx)[3];
				auto v1 = tile_corners({idx[0] - 1, idx[1]})[5];
				auto v2 = tile_corners({idx[0], idx[1] - 1})[1];

				auto c0 = tile_colors(idx);
				auto c1 = tile_colors({idx[0] - 1, idx[1]});
				auto c2 = tile_colors({idx[0], idx[1] - 1});

				c0 = gfx::dark(c0, 0.5f);
				c1 = gfx::dark(c1, 0.5f);
				c2 = gfx::dark(c2, 0.5f);

				triangles.push_back({{ {v0, n, c0}, {v1, n, c1}, {v2, n, c2} }});
			}

			if (idx[0] + 1 < map_.width())
			{
				auto v0 = tile_corners(idx)[0];
				auto v1 = tile_corners(idx)[5];
				auto v2 = tile_corners({idx[0] + 1, idx[1]})[2];
				auto v3 = tile_corners({idx[0] + 1, idx[1]})[3];

				auto c0 = tile_colors(idx);
				auto c1 = tile_colors({idx[0] + 1, idx[1]});

				c0 = gfx::dark(c0, 0.5f);
				c1 = gfx::dark(c1, 0.5f);

				triangles.push_back({{ {v0, n, c0}, {v1, n, c0}, {v2, n, c1} }});
				triangles.push_back({{ {v2, n, c1}, {v1, n, c0}, {v3, n, c1} }});
			}

			if (idx[1] + 1 < map_.height())
			{
				auto v0 = tile_corners(idx)[1];
				auto v1 = tile_corners(idx)[0];
				auto v2 = tile_corners({idx[0], idx[1] + 1})[3];
				auto v3 = tile_corners({idx[0], idx[1] + 1})[4];

				auto c0 = tile_colors(idx);
				auto c1 = tile_colors({idx[0], idx[1] + 1});

				c0 = gfx::dark(c0, 0.5f);
				c1 = gfx::dark(c1, 0.5f);

				triangles.push_back({{ {v0, n, c0}, {v1, n, c0}, {v2, n, c1} }});
				triangles.push_back({{ {v2, n, c1}, {v1, n, c0}, {v3, n, c1} }});
			}

			if (idx[0] > 0 && idx[1] + 1 < map_.height())
			{
				auto v0 = tile_corners(idx)[2];
				auto v1 = tile_corners(idx)[1];
				auto v2 = tile_corners({idx[0] - 1, idx[1] + 1})[4];
				auto v3 = tile_corners({idx[0] - 1, idx[1] + 1})[5];

				auto c0 = tile_colors(idx);
				auto c1 = tile_colors({idx[0] - 1, idx[1] + 1});

				c0 = gfx::dark(c0, 0.5f);
				c1 = gfx::dark(c1, 0.5f);

				triangles.push_back({{ {v0, n, c0}, {v1, n, c0}, {v2, n, c1} }});
				triangles.push_back({{ {v2, n, c1}, {v1, n, c0}, {v3, n, c1} }});
			}
		}

		for (auto & t : triangles)
		{
			auto n = geom::normal(t[0].position, t[1].position, t[2].position);
			t[0].normal = n;
			t[1].normal = n;
			t[2].normal = n;
		}

		terrain_mesh_.setup(vertex::attributes());
		terrain_mesh_.load(triangles, gl::STATIC_DRAW);
	}

	void main_scene::update_water_mesh()
	{
		std::vector<geom::triangle<vertex>> triangles;

		float size = map_size * 10.f;

		auto map_center = tile_center({map_.width() / 2, map_.height() / 2});

		geom::vector<float, 3> n{0.f, 0.f, 1.f};
		gfx::color_rgba c = ungamma({0, 63, 191, 255});
		if (freeze_timer_ > 0.f)
			c = ungamma({127, 191, 255, 255});

		for (int i = 0; i < 6; ++i)
		{
			auto v0 = geom::point<float, 3>::zero() + geom::swizzle<0, 1, -1>(map_center);
			v0[2] = water_height_;

			auto v1 = v0 + geom::swizzle<0, 1, -1>(geom::rotate(geom::vector{1.f, 0.f}, geom::rad(i * 60.f       ))) * size;
			auto v2 = v0 + geom::swizzle<0, 1, -1>(geom::rotate(geom::vector{1.f, 0.f}, geom::rad(i * 60.f + 60.f))) * size;

			triangles.push_back({{ {v0, n, c}, {v1, n, c}, {v2, n, c} }});
		}

		water_mesh_.setup(vertex::attributes());
		water_mesh_.load(triangles, gl::STATIC_DRAW);
	}

	void main_scene::update_hightlighted_tiles()
	{
		highlighted_tiles_.clear();

		if (!current_card_ || !applying_card_)
			return;

		for (auto idx : map_.indices())
		{
			geom::point<int, 2> p{idx[0], idx[1]};
			bool good = false;

			auto const & tile = map_(idx);

			if (tile.elevation <= water_level_) continue;

			auto has_water_nearby = [&]{
				for (auto n : neighbours)
				{
					auto q = p + n;
					if (q[0] < 0 || q[1] < 0 || q[0] >= map_.width() || q[1] >= map_.height()) continue;

					if (map_(q[0], q[1]).elevation <= water_level_)
						return true;
				}

				return false;
			};

			switch (*current_card_)
			{
			case card_type::build_farm:
				good = !tile.building && (tile.terrain == terrain_type::grassland || tile.terrain == terrain_type::forest);
				break;
			case card_type::build_fishing_hut:
				good = !tile.building && has_water_nearby();
				break;
			case card_type::build_woodcutter:
				good = !tile.building && tile.terrain == terrain_type::forest;
				break;
			case card_type::build_stone_quarry:
				good = !tile.building && tile.terrain == terrain_type::stone;
				break;
			case card_type::build_temple:
				good = !tile.building;
				break;
			case card_type::hill_1:
			case card_type::hill_2:
			case card_type::hill_3:
				good = true;
				break;
			default:
				break;
			}

			if (good)
				highlighted_tiles_.insert({idx[0], idx[1]});
		}
	}

	struct app_impl
		: app::app
	{
		app_impl()
			: app::app("Atlantis", 4)
		{
			push_scene(std::make_shared<main_scene>());
		}
	};

}

int main()
{
	return psemek::app::main<atlantis::app_impl>();
}

